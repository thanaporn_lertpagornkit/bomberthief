﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public bool isGamePlay = false;

    private int playerNumber;

    //Bomb
    [SerializeField] Transform itemHoldTransform;
    private float force = 500; //to Throw Bomb;
    
    //Movement
    private InputControl inputControl;
    private PlayerMovement playerMovement;

    private void Awake()
    {
        inputControl = GetComponent<InputControl>();
        playerMovement = GetComponent<PlayerMovement>();
    }

    public void Init(int playerNumber)
    {
        this.playerNumber = playerNumber;
        GetComponent<InputControl>().Init(playerNumber);
        playerMovement.DeAccel();
    }

    //private void Accel(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    //{
    //    if(obj.performed)
    //    {
    //        playerMovement.Accel();
    //    }
    //    else if (obj.canceled)
    //    {
    //        playerMovement.DeAccel();
    //    }
    //}


    private void Start()
    {
        CountDownAnimation.instance.OnCountEnd += OnPlay;
        GamePlayUIManager.instance.OnGameEnd += OnGameEnd;
        
    }

    private void Update()
    {
        if (isGamePlay == true)
        {
            if (playerNumber == 0)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                    playerMovement.Accel();
                else if(Input.GetKeyUp(KeyCode.Space))
                    playerMovement.DeAccel();

                if (Input.GetKeyDown(KeyCode.C) && GameManager.instance.BuyBomb(100, playerNumber))
                {
                    var bomb = Instantiate(Resources.Load("Prefab/Bomb_Model") as GameObject, itemHoldTransform.position, itemHoldTransform.rotation);
                    bomb.GetComponent<Rigidbody>().AddForce(transform.forward * force);
                }
            }
            else if (playerNumber == 1)
            {
                if (Input.GetKeyDown(KeyCode.Slash))
                    playerMovement.Accel();
                else if (Input.GetKeyUp(KeyCode.Slash))
                    playerMovement.DeAccel();

                if (Input.GetKeyDown(KeyCode.Semicolon) && GameManager.instance.BuyBomb(100, playerNumber))
                {
                    var bomb = Instantiate(Resources.Load("Prefab/Bomb_Model") as GameObject, itemHoldTransform.position, itemHoldTransform.rotation);
                    bomb.GetComponent<Rigidbody>().AddForce(transform.forward * force);
                }
            }
            inputControl.HandleAllInput();
        }
    }

    private void FixedUpdate()
    {
        playerMovement.HandleAllMovement();
    }

    private void OnPlay()
    {
        isGamePlay = true;
    }

    private void OnGameEnd()
    {
        isGamePlay = false;
    }

    public void GotBomb()
    {
        StartCoroutine(IEstunByBomb());
    }


    private IEnumerator IEstunByBomb()
    {
        isGamePlay = false;
        yield return new WaitForSeconds(3.0f);
        isGamePlay = true;
    }

    public void GetScore(int score)
    {
        GameManager.instance.ScoreUpdate(score, playerNumber);
    }

    
}
