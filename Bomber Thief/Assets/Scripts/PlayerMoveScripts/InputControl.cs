﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;


public class InputControl : MonoBehaviour
{
    private PlayerControls playerControls;
    
    public Vector2 movementInput;

    public float verticalInput;
    public float horizontalInput;


    public void Init(int playerSide)
    {
        switch (playerSide)
        {
            case 0:
                {
                    if (playerControls == null)
                    {
                        playerControls = new PlayerControls();
                        playerControls.PlayerGreenAction.Move.performed += i => movementInput = i.ReadValue<Vector2>();
                        playerControls.PlayerGreenAction.Move.canceled += i => movementInput = Vector3.zero;
                    }

                    playerControls.Enable();
                    break;
                }
            case 1:
                {
                    if (playerControls == null)
                    {
                        playerControls = new PlayerControls();
                        playerControls.PlayerPinkAction.Move.performed += i => movementInput = i.ReadValue<Vector2>();
                        playerControls.PlayerPinkAction.Move.canceled += i => movementInput = Vector3.zero;
                    }

                    playerControls.Enable();
                    break;
                }
            default:
                {
                    Debug.Log("Input Control | PlayerSide case: default");
                    break;
                }
        }
    }


    public void OnEnable()
    {
        if(playerControls != null)
        {
            playerControls.Enable();
        }
    }

    private void OnDisable()
    {
        playerControls.Disable();
    }

    public void HandleAllInput()
    {
        HandleMoveInput();
    }

    private void HandleMoveInput()
    {
        verticalInput = movementInput.y;
        horizontalInput = movementInput.x;
    }
}
