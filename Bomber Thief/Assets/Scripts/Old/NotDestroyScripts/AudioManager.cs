﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    private AudioSource audioSource;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        DontDestroyOnLoad(gameObject);
    }

    public void PlayAudio()
    {
        audioSource.Play();
    }

    public void ChangeAudio(string audioClipSrc)
    {
        audioSource.Pause();
        audioSource.clip = Resources.Load<AudioClip>(audioClipSrc);
        StartCoroutine(PlayDelay());
    }

    private IEnumerator PlayDelay()
    {
        yield return new WaitForSeconds(2.0f);
        audioSource.Play();
    }
    
}
