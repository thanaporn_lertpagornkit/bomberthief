﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class CountDownAnimation : MonoBehaviour
{
    public delegate void DelegateNoParameter();
    public event DelegateNoParameter OnCountEnd;
    public event DelegateNoParameter OnResult;

    private Vector2 onCountPosition = new Vector2(0, 78);
    private Vector2 noCountPosition = new Vector2(0, -500);
    private int countDownTime;
    private TextMeshProUGUI countDownText;

    public static CountDownAnimation instance;

    private Animator animator;

    private void Awake()
    {
        instance = this;
        transform.localPosition = noCountPosition;
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
        countDownText = GetComponent<TextMeshProUGUI>();
        countDownText.text = "";
        countDownTime = -1;
        GameManager.instance.OnGameStartCountDown += OnGameStartCountDown;
        GamePlayUIManager.instance.OnGameEnd += OnGameEnd;
    }

    

    private void OnGameStartCountDown()
    {
        countDownText.text = string.Empty;
        countDownTime = 3;
        transform.localPosition = onCountPosition;
        animator.SetTrigger("OnCountDown");
        countDownText.text = countDownTime.ToString();
    }

    private void ChangeText()
    {
        countDownTime--;
        if (countDownTime <= -1)
        {
            transform.localPosition = noCountPosition;
            //animator.SetBool("isCount", false);
        }
        else if (countDownTime == 0)
        {
            animator.SetTrigger("OnCountDown");
            countDownText.text = "Start";
            if (OnCountEnd != null)
                OnCountEnd();
            GameManager.instance.GameStart();
        }
        else
        {
            animator.SetTrigger("OnCountDown");
            countDownText.text = countDownTime.ToString();
        }
    }

    private void OnGameEnd()
    {
        animator.SetTrigger("OnEndGame");
        countDownText.text = "Game End";
        transform.localPosition = onCountPosition;
    }

    private void FinishGamePlay()
    {
        transform.localPosition = noCountPosition;
        //animator.SetTrigger("OnGameEnd");
        Debug.Log("FinishGamePlay");
        if (OnResult != null)
            OnResult();
    }

}
