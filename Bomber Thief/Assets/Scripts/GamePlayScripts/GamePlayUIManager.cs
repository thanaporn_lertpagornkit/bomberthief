﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GamePlayUIManager : MonoBehaviour
{
    public delegate void DelegateNoParameter();
    public event DelegateNoParameter OnStartTriggered;
    public event DelegateNoParameter OnGameEnd;
    public event DelegateNoParameter OnPlayAgain;
    public event DelegateNoParameter OnReStart;

    //Menu Panel
    [SerializeField] private GameObject menuPanel;
    [SerializeField] private Button menuStartButton;
    [SerializeField] private Button menuExitButton;

    //Ready State
    //[SerializeField] private GameObject countDownTimer;

    //InGame
    //UI
    [SerializeField] private GameObject gamePlayUI;
    private int timer = 60;
    [SerializeField] private Text timerText;
    private bool isStartButtonActive;
    [SerializeField] private Text[] UIScore;
    [SerializeField] private Button exitButton;

    //EndGame
    //[SerializeField] private GameObject gameEndText;

    //Result Panel
    [SerializeField] private GameObject resultPanel;
    [SerializeField] private Button buttonReStart;
    [SerializeField] private Button buttonExit_ResultPanel;
    [SerializeField] private GameObject winGreenImage;
    [SerializeField] private GameObject loseGreenImage;
    [SerializeField] private GameObject winPinkImage;
    [SerializeField] private GameObject losePinkImgae;
    [SerializeField] private Text scoreGreenText;
    [SerializeField] private Text scorePinkText;

    private int[] score;
    // Start is called before the first frame update

    public static GamePlayUIManager instance;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        menuPanel.SetActive(true);
        isStartButtonActive = false;

        gamePlayUI.SetActive(false);
        resultPanel.SetActive(false);

        menuStartButton.onClick.AddListener(GameStartTrigger);
        menuExitButton.onClick.AddListener(() => { Application.Quit(); });

        exitButton.onClick.AddListener(() => { Application.Quit(); });
        
        buttonReStart.onClick.AddListener(GameStartTrigger);
        buttonExit_ResultPanel.onClick.AddListener(() => { Application.Quit(); });

        GameManager.instance.OnGameStartCountDown += OnGameStartCountDown;
        GameManager.instance.OnGameStart += OnGameStart;
        CountDownAnimation.instance.OnResult += OnResult;


    }

    private void GamePlayInit()
    {
        score = new int[] { 100, 100 };
        for (int i = 0; i < score.Length; i++)
        {
            UIScoreUpdate(score[i], i);
        }
        menuPanel.SetActive(false);
        resultPanel.SetActive(false);
        gamePlayUI.SetActive(true);
    }


    private void Update()
    {
        
    }

    private void GameStartTrigger()
    {
        timerText.text = timer.ToString();
        GamePlayInit();
        if (OnStartTriggered != null)
            OnStartTriggered();
    }

    private void OnGameStartCountDown()
    {
        //resultPanel.SetActive(false);
        //menuPanel.SetActive(false);

        //gamePlayUI.SetActive(true);
    }

    private void OnGameStart()
    {
        StartCoroutine(GameTimer());
    }

    private IEnumerator GameTimer()
    {
        timerText.text = timer.ToString();
        WaitForSeconds waitForSeconds = new WaitForSeconds(1.0f);
        for(int i = timer; i >= 0; i--)
        {
            yield return waitForSeconds;
            timerText.text = i.ToString();
        }
        if (OnGameEnd != null)
            OnGameEnd();

        yield return new WaitForSeconds(3.0f);

        OnResult();
    }

    public void UIScoreUpdate(int scoreUpdate, int playerNumber)
    {
        Debug.Log("scoreUpdate " + scoreUpdate);
        score[playerNumber] = scoreUpdate;
        UIScore[playerNumber].text = score[playerNumber].ToString();
    }

    private void OnResult()
    {
        
        gamePlayUI.SetActive(false);
        scoreGreenText.text = score[0].ToString();
        scorePinkText.text = score[1].ToString();

        if(score[0] > score[1])
        {
            winGreenImage.SetActive(true);
            loseGreenImage.SetActive(false);
            winPinkImage.SetActive(false);
            losePinkImgae.SetActive(true);

        }
        else if(score[0] < score[1])
        {
            winGreenImage.SetActive(false);
            loseGreenImage.SetActive(true);
            winPinkImage.SetActive(true);
            losePinkImgae.SetActive(false);
        }
        else
        {
            winGreenImage.SetActive(false);
            loseGreenImage.SetActive(true);
            winPinkImage.SetActive(false);
            losePinkImgae.SetActive(true);
        }

        resultPanel.SetActive(true);
    }

}
