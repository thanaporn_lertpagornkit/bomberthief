﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombController : MonoBehaviour
{
    private void BrokenBomb()
    {
        Destroy(this.transform.parent.gameObject);
    }

    private void BombSoundPlay()
    {
        AudioSource.PlayClipAtPoint(Resources.Load<AudioClip>("Sound/Pubg_Bomb_Ringtone"), Camera.main.transform.position, 0.8f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            other.GetComponent<PlayerManager>().GotBomb();
        }
    }
}
