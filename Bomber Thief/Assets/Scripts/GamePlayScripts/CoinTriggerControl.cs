﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinTriggerControl : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            other.GetComponent<PlayerManager>().GetScore(100);
            //Destroy(this.transform.parent.gameObject);
            Destroy(gameObject);
        }
    }
}
