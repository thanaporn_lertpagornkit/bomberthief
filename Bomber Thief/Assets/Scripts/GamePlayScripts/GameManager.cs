﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public delegate void DelegateNoParameter();
    public event DelegateNoParameter OnGameStartCountDown;
    public event DelegateNoParameter OnGameStart;

    private int[] playerScore;

    [SerializeField] private GameObject coinPref;
    [SerializeField] private GameObject playerGreenPref;
    [SerializeField] private GameObject playerPinkPref;
    [SerializeField] private Transform spawmPointPlayerGreen;
    [SerializeField] private Transform spawmPointPlayerPink;

    private GameObject playerGreen;
    private GameObject playerPink;

    public string gameState;

    public static GameManager instance;

    private void Awake()
    {
        instance = this;
        gameState = "waitingReady";
    }

    private void Start()
    {
        GamePlayUIManager.instance.OnStartTriggered += OnStartTrigger;
        GamePlayUIManager.instance.OnGameEnd += OnGameEnd;
        CountDownAnimation.instance.OnResult += OnResult;
    }

    

    public void Init()
    {
        playerScore = new int[] { 100, 100 };

        for(int i=0; i < playerScore.Length; i++)
            GamePlayUIManager.instance.UIScoreUpdate(playerScore[i], i);

        //Player Pref
        //playerGreen = Instantiate<GameObject>(playerGreenPref, spawmPointPlayerGreen.position, spawmPointPlayerGreen.rotation);
        //playerGreen.GetComponent<PlayerManager>().Init(0);
        //playerPink = Instantiate<GameObject>(playerPinkPref, spawmPointPlayerPink.position, spawmPointPlayerPink.rotation);
        //playerPink.GetComponent<PlayerManager>().Init(1);
        //Debug.Log("Init");

        playerGreen = Instantiate<GameObject>(playerGreenPref, new Vector3(UnityEngine.Random.Range(-9.5f, 10.0f), 0.6f, UnityEngine.Random.Range(12.0f, 26.0f)), spawmPointPlayerGreen.rotation);
        playerGreen.GetComponent<PlayerManager>().Init(0);
        playerPink = Instantiate<GameObject>(playerPinkPref, new Vector3(UnityEngine.Random.Range(-9.5f, 10.0f), 0.6f, UnityEngine.Random.Range(12.0f, 26.0f)), spawmPointPlayerPink.rotation);
        playerPink.GetComponent<PlayerManager>().Init(1);
        

        
    }

    private void Reset()
    {
        Init();
    }

    private void OnStartTrigger()
    {
        Init();

        Debug.Log("GameManager: OnStartTrigger");
        if (OnGameStartCountDown != null)
            OnGameStartCountDown();

        StartCoroutine(SpawnItem());
        StartCoroutine(BossCheck());
        
    }

    public void GameStart()
    {
        gameState = "InGame";
        if (OnGameStart != null)
            OnGameStart();
    }

    public void OnGameEnd()
    {
        gameState = "Result";
        StopAllCoroutines();
        Debug.Log("Result");
    }

    private void Update()
    {
        switch(gameState)
        {
            case "waitingReady":
                {
                    break;
                }
            case "InGame":
                {
                    break;
                }
            case "Result":
                {
                    break;
                }
            default:
                {
                    Debug.Log("switch: No case");
                    break;
                }
        }
    }

    private void ForceGameEnd()
    {
        gameState = "Result";
        Debug.Log("Result");
    }

    private IEnumerator SpawnItem()
    {
        while (true)
        {
            var spawnPointPosition = new Vector3(UnityEngine.Random.Range(-9.5f, 10.0f), 8.0f, UnityEngine.Random.Range(12.0f, 26.0f));
            Instantiate(coinPref, spawnPointPosition, Quaternion.identity);
            yield return new WaitForSeconds(UnityEngine.Random.Range(0f, 3.0f));
        }
    }

    private IEnumerator BossCheck()
    {

        yield return new WaitForSeconds(UnityEngine.Random.Range(0, 10.0f));
    }

    private void OnResult()
    {
        Destroy(playerGreen);
        Destroy(playerPink);
    }

    public void ScoreUpdate(int score, int playerNumber)
    {
        Debug.Log("score " + score);
        playerScore[playerNumber] += score;
        GamePlayUIManager.instance.UIScoreUpdate(playerScore[playerNumber], playerNumber);
    }

    public bool BuyBomb(int bombPrice, int playerNumber)
    {
        if (playerScore[playerNumber] >= bombPrice)
        {
            ScoreUpdate(-bombPrice, playerNumber);
            return true;
        }

        return false;
    }
}
