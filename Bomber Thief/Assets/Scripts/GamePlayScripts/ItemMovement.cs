﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMovement : MonoBehaviour
{
    private bool haveOwner;
    private Rigidbody rb;
    private Transform ownerPickUpPointTransform;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if(haveOwner == true && ownerPickUpPointTransform != null)
        {
            transform.position = ownerPickUpPointTransform.position;
        }
    }

    public void SetOwner(Transform pickUpPoint)
    {
        ownerPickUpPointTransform = pickUpPoint;
        rb.Sleep();
        
        haveOwner = true;
    }

    public void OffOwner()
    {
        rb.WakeUp();
        haveOwner = false;
        ownerPickUpPointTransform = null;
    }

    public void WasUsed()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (ownerPickUpPointTransform != null)
        {
            if (other.CompareTag("Player"))
            {

            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (ownerPickUpPointTransform != null)
        {
            if (other.CompareTag("Player"))
            {

            }
        }
    }

}
